CREATE TABLE accounts(
    id SERIAL PRIMARY KEY,
    name TEXT
);

INSERT INTO accounts(name) VALUES ('test');